module SignInHelper
  include Capybara::DSL

  def sign_in_as(usr)
    visit new_user_session_path
    fill_in "Email", with: usr.email
    fill_in "Password", with: usr.password
    click_button("Sign in")

    self

  end
end