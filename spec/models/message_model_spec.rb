require 'rails_helper'
RSpec.describe Message, type: :model do

  it { expect belong_to(:user)}
  it { expect validate_presence_of(:user)}
  it { expect validate_presence_of(:content)}
end