require 'rails_helper'
RSpec.describe Comment, :type => :model do
it { expect belong_to(:user)}
it { expect belong_to(:recipe)}
it { expect validate_presence_of(:user)}
it { expect validate_presence_of(:recipe)}
it { expect validate_length_of(:description).is_at_least(4)}
it { expect validate_length_of(:description).is_at_most(140)}

end