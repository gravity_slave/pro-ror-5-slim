RSpec.describe Ingredient, :type => :model do

  it { expect validate_presence_of(:name)}
  it { expect have_many(:recipe_ingredients)}
  it { expect have_many(:recipe) }
  it { expect validate_length_of(:name).is_at_least(5) }
  it { expect validate_length_of(:name).is_at_most(25) }
  it { expect validate_uniqueness_of(:name)}

end