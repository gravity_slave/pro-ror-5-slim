RSpec.describe Recipe, type: :model do


      let(:recipe) {FactoryGirl.build(:recipe) }

      it { expect belong_to(:user) }

      it { expect have_many(:recipe_ingredients) }
      it { expect have_many(:ingredients) }

    it "should be  valid " do

        expect(recipe).to be_valid
    end

    it "should not be  valid " do

      recipe.name = ""
      expect(recipe).to_not be_valid


    end

    it 'should not be valid with  description less than 5 letters' do
    recipe.description = "deus"
    expect(recipe).to_not be_valid
  end

  it 'should not be valid with  description greater than 500 letters' do
  recipe.description = "deus vult" *130
  expect(recipe).to_not be_valid

end

it 'should not be valid withoud chef' do
  recipe.user = nil
  expect(recipe).to_not be_valid

end



end