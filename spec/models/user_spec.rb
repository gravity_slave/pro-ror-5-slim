require 'rails_helper'

RSpec.describe User, type: :model  do

  describe "User model" do
      let(:user) {FactoryGirl.build(:user)}
      it { expect validate_presence_of(:name)}

    it "should be valid" do

    expect(user).to be_valid

    end

      it 'should not be valid without name' do
      user.name = ""
      expect(user).to_not be_valid

    end

    it 'should not be valid with name more than 30 charactes' do
      user.name = " non nobis domine" *4
      expect(user).not_to be_valid
    end


      it 'should not be valid withour email' do
        user.email = ""
        expect(user).to_not be_valid
      end
      it 'should not be valid without email too long' do
        user.email = "@"*245 + "@wewilltake.herusalem"
        expect(user).to_not be_valid
      end

    it 'email should be valid with correct form', valids: true do
      valid_emails = %w[ user@example.com GRAVITYSLAVE@gmail.com R.first@yahoo.ru john+smith@co.uk.org ]
      valid_emails.each do |valids|
        user.email = valids
        expect(user).to be_valid
        puts "#{valids.inspect} should be valid"
      end
    end
      it 'email should not be valid with correct form', valids: true do
        valid_emails = %w[ user@example GRAVITYSLAVE@gmail,com R.first@yahoo. john+smith@+co.org ]
        valid_emails.each do |valids|
          user.email = valids
          expect(user).to_not be_valid
          puts "#{valids.inspect} should not be valid"
        end
      end

    it 'email should be uniq and case insensitive' do
    duplicate_user = user.dup
      duplicate_user.email =  user.email.upcase
      user.save
      expect(duplicate_user).to_not be_valid
    end


    it 'email should be lower case before hitting db', lcase: true do
      mixed_email = 'JohN@Example.com'
      user.email = mixed_email
      user.save
      mixed_email.downcase.should  eq(user.email)
      puts user.email
    end

    it 'must have password' do
    user.password = user.password_confirmation = " "
    expect(user).to_not be_valid
    end
      it 'must have appropriate password length' do
        user.password = user.password_confirmation = "pynia"
        expect(user).to_not be_valid
      end

end
end
