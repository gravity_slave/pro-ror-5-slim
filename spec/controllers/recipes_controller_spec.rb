require 'rails_helper'
RSpec.describe RecipesController, type: :controller do



  let(:user) { FactoryGirl.create(:user, email: "wewil@take.jerusalem")}
  let(:recipe) { FactoryGirl.create(:recipe, user: user) }
  before { sign_in(user) }

    it "should render index" do

   get :index

   expect(response).to render_template(:index)

end

    it 'should render show' do

      get :show, id: recipe

      expect(response).to render_template :show
    end

    it 'should render new' do
      get :new

      expect(response).to render_template :new
    end

    it 'creates a new valid recipe' do
     expect {
      post :create, :recipe => {name: "deusvult", description: "this should pass", user: user}
     }.to change(Recipe, :count).by(1)

      expect(flash[:notice]).to match("The recepy was successfully created")
      expect(response).to redirect_to(recipe_path(Recipe.first.id))



    end

    it 'rejects invalid recipe submission' do
      post :create,  :recipe => {name: " ", description: " "}
      expect(Recipe.count).to eq(0)

      expect(response).to render_template :new


    end


      it 'should render edit' do
        get :edit, id: recipe
        expect(response).to render_template :edit
      end



   it 'edits with valid recipe' do

     patch :update,id: recipe ,:recipe => {name: "deusvult", description: "this should pass"}

     expect(response).to redirect_to(recipe_path(recipe))

     expect(assigns(:recipe).name).to eq "deusvult"
     expect(assigns(:recipe).description).to eq("this should pass")

   end

   it 'rejects invalid recipe submission' do

     patch :update, id: recipe, :recipe => {name: " ", description: " "}

     expect(response).to render_template :edit
   end

  it 'should delete' do
    @recipe = FactoryGirl.create(:recipe, user: user)
    expect {
      delete :destroy, id: @recipe

    }.to change(Recipe, :count).from(1).to(0)
  end

  it "should delete comments" do
    # @recipe = FactoryGirl.create(:recipe, user: @chef)
    @comment1 = FactoryGirl.create(:comment, user: user, recipe: recipe)
    @comment1 = FactoryGirl.create(:comment, user: user, recipe: recipe)
    expect{
      delete :destroy, id: recipe.id
    }.to  change(Comment, :count).by(-2)
  end
  end

