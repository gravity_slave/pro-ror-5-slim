require 'rails_helper'

RSpec.describe  CommentsController, type: :controller do

  it 'should create comment with valid date' do
  @user = FactoryGirl.create(:user)
  @recipe = FactoryGirl.create(:recipe, user: @user)
  @comment = FactoryGirl.attributes_for(:comment,user: @user, recipe: @recipe)
  sign_in(@user)
  expect{
    post :create, recipe_id: @recipe.id, comment: @comment

  }.to change(Comment, :count).by(1)
  end


end