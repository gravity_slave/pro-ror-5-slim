require 'rails_helper'
RSpec.describe  MessagesController, type: :controller do
it 'should  create comment' do
   @user = FactoryGirl.create(:user)
  sign_in(@user)
  @message = FactoryGirl.attributes_for(:message)
   expect{
     post :create, message: @message
   }.to change(Message, :count).by(1)
end
  end