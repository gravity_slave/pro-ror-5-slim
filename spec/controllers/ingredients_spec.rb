RSpec.describe IngredientsController, :type => :controller do
  let(:ingredient) { FactoryGirl.create(:ingredient)}
  let(:admin_user) { FactoryGirl.create(:admin_user) }


  describe "INDEX action" do
    before { get :index}

    it { expect render_template(:index)}

  end

  describe 'SHOW action' do

    before do
      get :show, id: ingredient


    end
    it { expect render_template(:show)}
  end

  describe 'EDIT action' do

    before do
      sign_in(admin_user)

      get :edit, id: ingredient
    end


    it { expect render_template('edit')}

  end

  describe 'Update action' do
    before {sign_in(admin_user) }

    it 'should assert differemce' do
    expect {
      post :create, ingredient: { name: "Justin Bieber"}
    }.to change(Ingredient, :count).by(1)
      expect(flash[:notice]).to eq("You hav successfully created ingredient")
    end



  end




  describe 'NEW action' do
    before do
      sign_in(:admin_user)
      get :new
    end

    it { expect render_template :new}

  end


end