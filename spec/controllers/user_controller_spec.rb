require 'rails_helper'

RSpec.describe "Devise User controller", type: :feature do

  it 'should reject invalid sign up' do
    post user_registration_path, user: {name: "", email: "", password: "", password_confirmation: ""}
    expect(User.count).to eq(0)


  end

  it 'should accept valid signup'do

    expect {
      post user_registration_path, user: {name: "Justinbieber", email: "justin@bieber.com", password: "asdfasdf", password_confirmation: "asdfasdf"}

    }.to change(User, :count).by(1)
    expect(flash[:notice]).to match("Welcome! You have signed up successfully.")
  end


end
