require 'rails_helper'
RSpec.describe UsersController, :type => :controller do

  before do
    @chef= FactoryGirl.create(:user)
    sign_in(@chef)
  end
  it "should delte user" do
    expect {
      delete :destroy, id: @chef.id

    }.to change(User, :count).by(-1)

  end

  it "should delete recipes" do

    @recipe1 =FactoryGirl.create(:recipe, user: @chef)
    @recipe2 = FactoryGirl.create(:another_recipe, user: @chef)
    expect{
      delete :destroy, id: @chef.id
    }.to  change(Recipe, :count).by(-2)
    expect(flash[:notice]).to match("#{@chef.name} has canceled his account")
  end

  it "should delete comments" do
    @recipe = FactoryGirl.create(:recipe, user: @chef)
  @comment1 = FactoryGirl.create(:comment, user: @chef, recipe: @recipe)
  @comment1 = FactoryGirl.create(:comment, user: @chef, recipe: @recipe)
  expect{
    delete :destroy, id: @chef.id
  }.to  change(Comment, :count).by(-2)

  end




end