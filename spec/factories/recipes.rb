FactoryGirl.define do
  factory :recipe do
  name Faker::Pokemon.name
  description "MY SNAPPY DESCRIPTION"
   user
  end
  factory :another_recipe, class: Recipe do
    name "DARTH VEIDER"
    description "ICH BIN UBER ALLES"
    user
  end
end