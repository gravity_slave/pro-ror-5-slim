require 'rails_helper'

RSpec.describe "Index And Show", type: :feature do

 before do
   @user = FactoryGirl.create(:admin_user)
   @recipe = FactoryGirl.create(:recipe, user: @user)
   @recipe2 = @user.recipes.build(FactoryGirl.attributes_for(:another_recipe))
   @recipe2.save
   sign_in_as(@user)
 end
 it "should list users" do
   visit chef_path(@user)
   expect(page).to have_content(@user.name)
   expect(page).to have_link(@recipe.name, recipe_path(@recipe))
   expect(page).to have_link(@recipe2.name, recipe_path(@recipe2))
 end


  it 'should render' do

    visit chef_path(@user)
    expect(page).to have_http_status :success
    expect(page).to have_current_path(chef_path(@user))

  end





end