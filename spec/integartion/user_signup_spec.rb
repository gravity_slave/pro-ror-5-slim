require 'rails_helper'

RSpec.describe  "Sign Up, In and Out ", type: :feature do

  describe "Sign up" do

    it 'should GET sign up path' do
      get new_user_session_path
      expect(response).to have_http_status :success
    end

    it "should render errors" do
      visit new_user_registration_path

      fill_in "Name", with: ""
      fill_in "Email", with: "dsads"
      fill_in "Password", with: "asdf"
      fill_in "user[password_confirmation]", with: "asd"
      click_button("Sign up")
      expect(page).to have_css('div.alert.alert-danger.alert-block.devise-bs')
    end

    it "should procede correctly" do
      visit new_user_registration_path

      fill_in "Name", with: "Justin Bieber"
      fill_in "Email", with: "wewiil@take.jerusalem"
      fill_in "Password", with: "asdfasdf"
      fill_in "user[password_confirmation]", with: "asdfasdf"
      click_button("Sign up")
      expect(page).to have_current_path(chef_path(User.last.id))

    end



  end

end