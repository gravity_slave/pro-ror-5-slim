require 'rails_helper'
RSpec.describe 'Delete user', type: :feature do

  before do
    @chef = FactoryGirl.create(:user, email: 'asdf@asdf.asdf', admin: true)
   sign_in_as(@chef)
  end

  it 'should delete user from db' do



    delete destroy_chef_path(@chef)
    expect(page).to redirect_to chefs_path



  end


end