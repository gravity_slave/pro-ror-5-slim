require 'rails_helper'

RSpec.describe "Listing chefs" , type: :feature do

  it "should render index" do

  get chefs_path
  expect(response).to have_http_status :success

  end

  it "should render content" do
    @chef = FactoryGirl.create(:user)
    @recipe1 = FactoryGirl.create(:recipe, user: @chef)
    @recipe2 =FactoryGirl.create(:another_recipe, user: @chef)
    visit chefs_path
    expect(page).to have_current_path(chefs_path)
    expect(page).to have_link(@chef.name, chef_path(@chef))
    expect(page).to have_content("#{@chef.recipes.count} recipes")

  end


end
