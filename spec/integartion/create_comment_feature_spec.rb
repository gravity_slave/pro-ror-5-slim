require 'rails_helper'

RSpec.describe " Post a comment", :type => :feature do

  it 'should remder a comment' do
    @user = FactoryGirl.create(:user)
    @recipe = FactoryGirl.create(:recipe, user: @user)
    sign_in_as(@user)
    visit recipe_path(@recipe)
    @comment ="comment"
    fill_in "Comment", with: @comment
    click_on("Submit Comment")
      expect(page).to have_current_path(recipe_comments_path(@recipe))

    expect(page).to have_content(@comment)
  end


end