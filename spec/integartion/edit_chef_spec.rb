require 'rails_helper'

RSpec.describe " Edit chefs", type: :feature do

context 'with invalid credentials' do
before do
  @chef = FactoryGirl.create(:user, email:"ich@ni.san")
  sign_in_as(@chef)

end
  it 'rejects an  edit' do

    get edit_chef_path(@chef)
    patch patch_chef_path(@chef), user: { name: "", email: "", password: "asd",password_confirmation: ""}
    expect(page).to render_template('edit')

  end

  it 'should render errors',edit_chef: true do

    visit edit_chef_path(@chef)
    fill_in "Name", with: ""
    fill_in "Email", with: ""
    click_button("Edit profile")
    expect(page).to have_http_status :success
    expect(page).to have_css('h2.panel-title')

    expect(page).to have_css('div.panel-body')



  end

  end

  context 'with valid credentials' do
    before do
      @chef = FactoryGirl.create(:user, email:"ich@ni.san")
      visit new_user_session_path
      fill_in "Email", with: @chef.email
      fill_in "Password", with: @chef.password
      click_button("Sign in")
    end
    it 'accepts an  edit' do
      get edit_chef_path(@chef)
      @new_name = "Rocky Balboa"
      @new_email = "iwill@punch.you"
      patch patch_chef_path(@chef), user: { name: @new_name, email: @new_email, password: @chef.password,password_confirmation: @chef.password_confirmation}
      expect(page).to redirect_to(chef_path(@chef))
      expect(assigns(:user).name).to eq(@new_name)
      expect(assigns(:user).email).to eq(@new_email)
    end

    it 'should display correct data' do


      visit edit_chef_path(@chef)

      fill_in "Name", with: "Hustin Bieber"
      fill_in "Email", with: "wewill@take.herusalem"
      click_button("Edit profile")


      expect(page).to have_http_status :success
      expect(page).to have_current_path(chef_path(@chef))
      expect(page).to have_content("You have successfully edited your profile")

    end

  end

  context 'as admin user' do

    before do
      @chef = FactoryGirl.create(:user, email:"ich@ni.san")
      @chef2 = FactoryGirl.create(:user, email: "ni@ich.san")
      @admin_chef = FactoryGirl.create(:admin_user, email: "sangorok@niich.ich")
    end

    it "accomplish edit attempt by admin" do

      sign_in_as(@admin_chef)


      visit edit_chef_path(@chef)
      fill_in "Name", with: "Justin Bieber"
      fill_in "Email", with: "admin@edited.this"
      click_button("Edit profile")
      @chef.reload
      expect(@chef.name).to eq("Justin Bieber")
      expect(@chef.email).to eq("admin@edited.this")
    end
    it "reject edit attempt by non-admin" do

      sign_in_as(@chef2)
      visit edit_chef_path(@chef)
      expect(page).to have_current_path(chefs_path)

    end

  end
end



