require 'rails_helper'

RSpec.feature 'Integearion test for pages', type: :feature do


    context "get home page" do
      before { get pages_home_path }

      it " expect  that status must be postive" do
        expect(response.status).to eq(200)
      end


    end

   context "get root" do
      before { get root_path }
      it { expect(response).to have_http_status(200) }
    end

end