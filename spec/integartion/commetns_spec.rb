require 'rails_helper'
RSpec.describe "Comments", :type => :feature do

  let(:user) { FactoryGirl.create(:user) }
  let(:recipe)  { FactoryGirl.create(:recipe, user: user)}


  it 'shoul do smt' do
    @comment2 = Comment.create(description: "Ich bin uber alles", user: user, recipe: recipe)
    @comment1 =Comment.create(description: "we are number one", user: user, recipe: recipe)
    visit recipe_path(recipe)
    expect(page).to have_current_path(recipe_path(recipe))

    expect(page).to have_content(@comment1.description)
    expect(page).to have_content(@comment2.description)

  end

end