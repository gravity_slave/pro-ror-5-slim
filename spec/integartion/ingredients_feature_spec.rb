require 'rails_helper'
RSpec.describe "Ingredient",type: :integration do
  before do
    @ingredient1 = FactoryGirl.create(:ingredient)
    @ingredient2 = FactoryGirl.create(:ingredient,name: "Noyou")

  end
context "display index" do


  it 'with correct content' do

    visit "/ingredients"
expect(page).to have_http_status :success

    expect(page).to  have_content(@ingredient1.name.capitalize)
    expect(page).to have_content(@ingredient2.name.capitalize)

  end
end

  context "display show" do

    it 'should render correctly' do
      visit "/ingredients/#{@ingredient1.id}"
      expect(page).to have_http_status :success
      expect(page).to have_content(@ingredient1.name)

end

  end


end