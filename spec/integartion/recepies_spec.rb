require 'rails_helper'


RSpec.describe  "Listing Articles", :type => :feature do

  describe "should render content" do

 before do
  @user = FactoryGirl.create(:user)
  @recipe = FactoryGirl.create(:recipe, user: @user)
   @recipe2 = @user.recipes.build(name: "my oishi fish", description: "istoo oishi")
    @recipe2.save
  sign_in_as(@user)
 end


   it "with articles created and user not signed in" do

     visit recipes_path


     expect(page).to have_link(@recipe.name, recipe_path(@recipe.id))
     expect(page).to have_link(@recipe2.name, recipe_path(@recipe2.id))

   end

    it "should render show page" do

      visit  recipe_path(@recipe)
      expect(page).to have_content(@recipe.name)
      expect(page).to have_content(@recipe.description)
      expect(page).to have_content(@recipe.user.name.downcase.capitalize)
      expect(page).to have_link("Edit recipe", edit_recipe_path(@recipe))
      expect(page).to have_link("Delete recipe", recipe_path(@recipe))
      expect(page).to have_link("Return to recipes listing", recipes_path)


    end
 it 'rejects invalid recipe submission' do
   visit new_recipe_path
   @name ="justin bieber"
   @description = "Non nobis domine! Deus VULT!"
   fill_in "Name", with: @name
   fill_in "Description", with: @description
   click_button "Create Recipe"
   expect(page).to have_content(@name.capitalize)
   expect(page).to have_content(@description)




 end

 it 'rejects invalid recipe submission' do
   visit new_recipe_path
    fill_in "Name", with: " "
   fill_in "Description", with: ""
   click_button "Create Recipe"
   expect(page).to have_css("div.panel-body")
   expect(page).to have_css("h2.panel-title")



 end

    it 'should edit correctly' do
      @ingredient = FactoryGirl.create(:ingredient)
      visit edit_recipe_path(@recipe)
      expect(page).to have_content(@ingredient.name)
      @name ="justin bieber"
      @description = "Non nobis domine! Deus VULT!"
      fill_in "Name", with: @name
      fill_in "Description", with: @description
      check("recipe_ingredient_ids_#{@ingredient.id}")
      click_button "Update Recipe"
      expect(page).to have_content(@name.capitalize)
      expect(page).to have_content(@description)
      expect(page).to have_content(@ingredient.name)
      expect(page).to have_content("The recipe was updated")


    end

 it 'should create correctly' do
   @ingredient = FactoryGirl.create(:ingredient)
   @ingredient2 = FactoryGirl.create(:ingredient, name: "oh_you")

   visit new_recipe_path(@recipe)
   expect(page).to have_content(@ingredient.name)
   expect(page).to have_content(@ingredient2.name)


   @name ="justin bieber"
   @description = "Non nobis domine! Deus VULT!"
   fill_in "Name", with: @name
   fill_in "Description", with: @description
   check("recipe_ingredient_ids_#{@ingredient.id}")

   check("recipe_ingredient_ids_#{@ingredient2.id}")
   click_button "Create Recipe"


   expect(page).to have_content(@name.capitalize)
   expect(page).to have_content(@description)
   expect(page).to have_link(@ingredient.name,ingredient_path(@ingredient))
   expect(page).to have_link(@ingredient2.name, ingredient_path(@ingredient2))


end

    it 'should not edit' do
      visit edit_recipe_path(@recipe)
      fill_in "Name", with: " "
      fill_in "Description", with: ""
      click_button "Update Recipe"
      expect(page).to have_css("div.panel-body")
      expect(page).to have_css("h2.panel-title")

    end

    it 'should delete' do
      visit recipe_path(@recipe)
      click_link("Delete recipe")
      expect(page).to have_current_path(recipes_path)
      expect(page).to have_content("was successfully deleted")
    end

  end

end


