class RecipesController < ApplicationController
  before_action :require_user_for_like, :only => [:like]
  before_action :authenticate_user!, :only => [:new, :edit, :destroy]
  prepend_before_action  :set_recipe, only:  [:show, :edit, :update, :destroy, :like]

  before_filter :check_chef, only: [:edit, :destroy]
  def index
    @recipes = Recipe.paginate(:page => params[:page], per_page: 5)
  end

  def show
    @comment = Comment.new
    @comments = @recipe.comments.paginate(:page => params[:page], per_page: 5)
  end

  def new
    @recipe = Recipe.new

  end


  def create
    @recipe = Recipe.new(recipe_params)
    @recipe.user = current_user
    if @recipe.save
      redirect_to @recipe, notice: 'The recepy was successfully created'
    else
      render :new
    end
  end


  def edit

  end

  def update
    @recipe.update_attributes(recipe_params)
    if @recipe.save
      redirect_to @recipe, notice: "The recipe was updated"
    else
      render 'edit'
    end
  end

  def destroy
    if @recipe.destroy
      redirect_to recipes_path, notice: "#{@recipe.name} was successfully deleted"
    end
  end

  def like
    like = Like.create(like: params[:like], user: current_user, recipe: @recipe)
    if like.valid?
      flash[:success] = "Your selection was succesful"
      redirect_to :back
    else
      flash[:danger] = "You can only like/dislike a recipe once"
      redirect_to :back
    end
  end
  private
  def require_user_for_like
    unless user_signed_in?
      redirect_to :back, :alert => "You must be logged in to perform that action"

    end
  end

  def check_chef
    unless @recipe.user == current_user || current_user.admin?
      redirect_to recipes_path, notice: "You must be an owner"
    end
  end

  def recipe_params
    params.require(:recipe).permit(:name, :description, :image,ingredient_ids: [])
  end

  def set_recipe
    @recipe =Recipe.find(params[:id])
  end
end