class IngredientsController < ApplicationController

  before_action :require_admin, except:  [:show, :index]
  before_action :set_ingredient, only: [:show, :edit, :update, :destroy]
  def index
    @ingredients = Ingredient.paginate(page: params[:page], per_page: 5)
  end

  def show
    @ingredient_recipes = @ingredient.recipes.paginate(page: params[:page], per_page: 5)

  end

  def new
    @ingredient = Ingredient.new
  end

  def create
    @ingredient = Ingredient.new(ingredient_params)
    if @ingredient.save
      redirect_to @ingredient, notice: "You hav successfully created ingredient"
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    @ingredient.update_attributes(ingredient_params)

  if @ingredient.save
    redirect_to @ingredient, notice: 'You have successfully updated  ingredient'
  else
    render 'edit'
  end
  end

  def destroy
  end

  private
  def require_admin
     unless user_signed_in? && current_user.admin?
       redirect_to ingredients_path, alert: "only  admin users can perform this action"
     end
  end

  def ingredient_params
    params.require(:ingredient).permit(:name)
  end

  def set_ingredient
    @ingredient = Ingredient.find(params[:id])
  end
end
