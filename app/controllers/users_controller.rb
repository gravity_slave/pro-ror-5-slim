class UsersController < ApplicationController
  prepend_before_action :set_user, only: [:show,  :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit]
  before_action :require_same_chef, only: [:edit]


  def index
    @users =User.paginate(page: params[:page], per_page: 5)


  end

  def show
    @chef_recipes = @user.recipes.paginate(page: params[:page], per_page: 5)


  end

  def edit

  end

  def update
    if @user.update(chefs_params)
      redirect_to chef_path(@user),  notice: "You have successfully edited your profile"
    else
      render 'edit'

    end
  end

  def destroy
    unless @user.admin?
      if @user.destroy
        redirect_to chefs_path, notice: "#{@user.name} has canceled his account"
      else
        render 'show'
      end
    else
      redirect_to chefs_path
    end

  end


  private

  def set_user
    @user = User.find(params[:id])

  end
  def require_admin
    unless current_user.admin?
      redirect_to chef_path(current_user), alert: "This action can be performed only by admin"
    end
  end

  def require_same_chef
    unless @user == current_user || current_user.admin?
      # flash[:alert] ="You must be the owner of the account to perform edit and delte actions"
      redirect_to chefs_path , alert: "You must be the owner of the account to perform edit and delte actions"

    end
  end

  def chefs_params
    params.require(:user).permit(:name,:email, :admin)
  end
end