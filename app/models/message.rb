class Message < ApplicationRecord
  belongs_to :user

  validates_presence_of :content, :user

  scope :most_recent, -> { order(:created_at).last(20)}

end
