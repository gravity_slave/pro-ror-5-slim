class User < ApplicationRecord
  before_save { self.email = email.downcase}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :recipes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :likes, dependent: :destroy


  validates_presence_of :name
  validates_length_of :name, maximum: 30
  validates_length_of :email, maximum: 255
  validates_uniqueness_of :email
  validates_presence_of :email, case_sensitive: false
  VALID_EMAILS_REGEX =  /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates_format_of :email, with: VALID_EMAILS_REGEX
end
