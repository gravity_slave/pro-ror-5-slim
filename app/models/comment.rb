class Comment < ApplicationRecord
  validates :description, presence: true, length: {
      :minimum => 4, :maximum => 140
  }
  belongs_to :user
  belongs_to :recipe
  validates_presence_of :user, :recipe

default_scope -> {order(:updated_at => :desc)}
end
