class Recipe < ApplicationRecord
  belongs_to :user
  has_many :recipe_ingredients
  has_many :ingredients, :through => :recipe_ingredients
  has_many :comments, :dependent => :destroy
  has_many :likes, dependent: :destroy

  mount_uploader :image, ImageUploader


  validates_presence_of :name, :description, :user
  validates_length_of :description, minimum: 5 ,maximum: 500
  default_scope -> {order(updated_at: :desc)}


  def thumbs_up_total
    self.likes.where(like: true).size
  end

  def thumbs_down_total
    self.likes.where(like: false).size
  end
end
