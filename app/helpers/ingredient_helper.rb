module IngredientHelper
  def is_new_ingredient?(ingredient)
    if ingredient.new_record?
      "Add ingredient"
    else
      "Update ingredient"
    end
  end
end
