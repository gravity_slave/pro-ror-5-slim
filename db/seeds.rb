# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.destroy_all
Ingredient.destroy_all
RecipeIngredient.destroy_all


User.create!(name: "BENEDIKT CUCUMBERBATCH", email: "nebiros90@mail.ru", password: "password", password_confirmation: "password", admin: true)
10.times do |i|
  Recipe.create(name: "LOREM IPSUM #{i}", description: "THIS IS MY LOREM IPSUM DESCRIPTION NUMBER #{i}", user: User.last)

end
puts "*************RECIPE********************"

10.times do |user|
  User.create!(name: "#{user}name", email: "#{user}user@mail.com", password: "password#{user}", password_confirmation: "password#{user}")
end
puts "*************USER********************"

10.times do |ing|
  Ingredient.create(name: "ingredient number #{ing}")
end
puts "*************INGREDIENT********************"

10.times do |ingre|
  RecipeIngredient.create(recipe: Recipe.find(ingre+1), ingredient: Ingredient.find(ingre+1))
end
puts "*************RECIPE**INGEDIENT********************"

10.times do |g|
  Ingredient.first.recipes << Recipe.find(g+1)
  Ingredient.first.save
end

puts "ADD RECIPES TO INGREDIENT ***************************8"


10.times do |c|
  Comment.create!(description: "#{c} comment", user:  User.first, recipe: Recipe.last)
end