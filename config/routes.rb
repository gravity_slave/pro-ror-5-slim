Rails.application.routes.draw do

  get 'chatrooms/show'

  get "/chat", to: "chatrooms#show"


  root 'pages#home'
  get 'pages/home', to: 'pages#home'

  devise_for :users
scope path: "/chefs", controller: :users do
 get '/' => :index, as: 'chefs'
  get "/:id" => :show, as: "chef"
  get "/:id/edit" => :edit, as: "edit_chef"
  patch "/:id" => :update, as: "patch_chef"
  delete "/:id" => :destroy, as: "destroy_chef"
end
  resources :messages, :only => [:create]
  resources :ingredients
  resources :recipes do
    resources :comments, :only =>  [:create]
    member do
      post 'like'
    end
  end
  mount ActionCable.server => '/cable'
end
